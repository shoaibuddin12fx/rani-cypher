import { Component } from '@angular/core';
import { NetworkService } from '../services/network.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  wallets = [];
  constructor(public network: NetworkService) {
    this.initialize();
  }

  async initialize() {
    let res = await this.network.getWallets();
    console.log(res);
    this.wallets = res.wallet_names;
  }

  async addWallet(){

    // get wallet addresses and make a choice
    const address = localStorage.getItem('addresses');
    if(!address){
      return;
    }

    const parsed = JSON.parse(address);

    // call add wallet here
    

    let n = new Date().getTime();
    const res = await this.network.createWallets({
      name: `W-${n}`,
      addresses: [
          parsed.address
      ]
    });

    this.initialize();
  }

  newTransaction(item){

    // add a value to address
    


  }

}
