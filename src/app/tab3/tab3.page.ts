import { Component } from '@angular/core';
import { NetworkService } from '../services/network.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor(public network: NetworkService) {}

  async addAmount(amount){
    
    console.log(amount);

    // call add transaction API

    let parsed = localStorage.getItem('addresses');

    if(!parsed){
      return;
    }

    const addresses = JSON.parse(parsed);
    const address = addresses.address;

    let data = {
        "inputs": [
            {
                "addresses": [
                    address
                ]
            }
        ],
        "outputs": [
            {
                "addresses": [
                    address
                ],
                "value": parseInt(amount)
            }
        ]
    }


    const res = await this.network.postNewTransaction(data);
    console.log(res);



  }

}
