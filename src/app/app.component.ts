import { Component } from '@angular/core';
import { NetworkService } from './services/network.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  
  constructor(public network: NetworkService) {
    this.initialize();
  }

  async initialize() {

    let addrs = localStorage.getItem('addresses');
    if(!addrs){

      const res = await this.network.getAddresses();
      console.log(res);

      let data = JSON.stringify(res);
      localStorage.setItem('addresses', data);

    }else{
      const res = JSON.parse(addrs);
      console.log(res);
    }

    
    

    




  }



}
