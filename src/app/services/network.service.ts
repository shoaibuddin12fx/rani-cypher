import { Injectable } from '@angular/core';
import { ApiService } from './api.service';


const tokens = require('src/app/data/tokens.json')

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  token = ''
  constructor(public api: ApiService, ) { 
    this.token = tokens.token;
  }

  getAddresses() {
    return this.httpPostResponse('addrs?token=' + this.token + '&bech32=true', {})
  }

  getWallets(){
    return this.httpGetResponse('wallets?token=' + this.token) 
  }

  createWallets(data){
    return this.httpPostResponse('wallets?token=' + this.token, data ) 
  }

  postNewTransaction(data){
    return this.httpPostResponse('txs/new?token=' + this.token, data );
  }








  // get transaction hashes
  getTransactionHashes(){
    console.log(this.token);

    // https://api.blockcypher.com/v1/btc/main/txs/f854aebae95150b379cc1187d848d58225f3c4157fe992bcd166f58bd5063449?token={{TOKEN}}'
  }


  serialize = ((obj) => {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  });


  httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
  }

  httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
  }

  httpPutResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return new Promise((resolve, reject) => {

      id = id ? `/${id}` : '';
      const url = key + id;

      this.api.put(key, data).subscribe((res: any) => {
        if (res.bool !== true) {
          if (showError) {
            console.log(res.message);
            // this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res.result);
        }
      });
    });
  }

  httpDeleteResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return new Promise((resolve, reject) => {
      this.api.delete(key).subscribe((res: any) => {
        console.log(res);
        if (res.bool !== true) {
          if (showError) {
            console.log(res.message);
            // this.utility.presentSuccessToast(res.message);
          }
          reject(null);
        } else {
          resolve(res.result);
        }
      });
    });
  }

  // default 'Content-Type': 'application/json',
  httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json'): Promise<any> {

    return new Promise((resolve, reject) => {

      if (showloader === true) {
        // this.utility.showLoader();
      }

      id = (id) ? '/' + id : '';
      const url = key + id;

      const seq = (type === 'get') ? this.api.get(url, {}) : this.api.post(url, data);

      seq.subscribe((res: any) => {
        if (showloader === true) {
          // this.utility.hideLoader();
        }

        // if (res.bool !== true) {
        //   if (showError) {
        //     console.log(res.message);
        //     // this.utility.presentSuccessToast(res.message);
        //   }
        //   reject(null);
        // } else {
          resolve(res);
        // }

      }, err => {

        const error = err.error;
        if (showloader === true) {
          // this.utility.hideLoader();
        }

        if (showError) {
          console.log(error.message);
          // this.utility.presentFailureToast(error.message);
        }

        console.log(err);

        // if(err.status === 401){
        //   this.router.navigate(['splash']);
        // }

        reject(null);

      });

    });

  }

}
